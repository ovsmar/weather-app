#Weather App
This is a simple Weather App , created with Python, Flask, Bulma , SQLAlchemy and Openweathermap 

Weather App est un projet de application de météo développé en utilisant Python, Flask, Bulma, SQLAlchemy et Openweathermap. Il permet à l'utilisateur de consulter les prévisions météorologiques pour n'importe quelle ville du monde en utilisant l'API Openweathermap. Le projet utilise également SQLAlchemy pour stocker les données météorologiques dans une base de données et également pour les supprimer et Flask pour mettre en place l'application Web.

![view](assets/view.gif)

